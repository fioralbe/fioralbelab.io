.PHONY: default clean

.DEFAULT_GOAL := default

clean:
	rm -rf build/public/ .parcel-cache/

default:
	npm run build